package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.ii.inf112.impl.AlignerA;
import no.uib.ii.inf112.impl.AlignerB;
import no.uib.ii.inf112.impl.AlignerC;
import no.uib.ii.inf112.impl.AlignerD;
import no.uib.ii.inf112.impl.AlignerE;
import no.uib.ii.inf112.impl.AlignerF;
import no.uib.ii.inf112.impl.AlignerG;
import no.uib.ii.inf112.impl.AlignerH;

public abstract class TestAligner {
	TextAligner aligner;

	@Test
	void testCenter() {
//		assertTrue(aligner.center("A", 5).strip().length() > 0);
//		assertTrue(aligner.center("", 5).length() > 0);
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals("  AA ", aligner.center("AA", 5));

//		assertTrue(aligner.center("AAA", 5).strip().length() < aligner.center("AAA", 5).length());
	}

	@Test
	void testFlushRight() {
//		assertTrue(aligner.flushRight("A", 5).strip().length() > 0);
//		assertTrue(aligner.flushRight("", 5).length() > 0);
		assertEquals("    A", aligner.flushRight("A", 5));

	}

	@Test
	void testFlushLeft() {
//		assertTrue(aligner.flushLeft("A", 5).strip().length() > 0);
//		assertTrue(aligner.flushLeft("", 5).length() > 0);
		assertEquals("A    ", aligner.flushLeft("A", 5));

	}

	@Test
	void testJustify() {
//		assertTrue(aligner.justify("A", 5).strip().length() > 0);
//		assertTrue(aligner.justify("", 5).length() > 0);
		assertEquals("foo  foo  bar", aligner.justify("foo foo bar", 13));

	}
}

class TestAlignerA extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerA();
	}
}

class TestAlignerB extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerB();
	}
}

class TestAlignerC extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerC();
	}
}

class TestAlignerD extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerD();
	}
}

class TestAlignerE extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerE();
	}
}

class TestAlignerF extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerF();
	}
}

class TestAlignerG extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerG();
	}
}

class TestAlignerH extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerH();
	}
}